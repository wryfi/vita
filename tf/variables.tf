variable "project" {
  type = string
  description = "project id (required)"
}

variable "image" {
  type = string
  description = "docker image name (required)"
}

variable "tag" {
  type = string
  description = "docker image tag (required)"
}

variable "domain" {
  type = string
  description = "mapped domain (required)"
}

variable "bucket" {
  type = string
  description = "vita_gcp_bucket (required)"
}

variable "bucket_location" {
  type = string
  description = "required"
}

variable "vita_json_vitals_url" {
  type = string
  description = "vita_json_vitals_url (required)"
}

variable "vita_file_basename" {
  type = string
  description = "vita_file_basename (required)"
}

variable "service_name" {
  type = string
  description = "cloud run service name"
  default = "vita"
}
