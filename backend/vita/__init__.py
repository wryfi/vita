import logging

from flask import Flask, request, redirect, send_from_directory, Response
from whitenoise import WhiteNoise

from vita.format import render_html, render_markdown
from vita.settings import config
from vita.storage import get_download, get_json

level = {
    "debug": logging.DEBUG,
    "info": logging.INFO,
    "warning": logging.WARNING,
    "error": logging.ERROR,
    "critical": logging.CRITICAL,
}.get(config.get("log.level"), logging.WARNING)

logging.basicConfig(level=level, format="[%(levelname)s]  %(module)s  -  %(message)s")


def create_app(debug=False):
    app = Flask(__name__)
    app.debug = debug
    # whitenoise will serve our react app and assets
    app.wsgi_app = WhiteNoise(app.wsgi_app, root=config.get("react.path"), index_file=True)

    @app.route("/file")
    def download():
        file_type = request.args.get("format", "pdf")
        download_url = get_download(file_type)
        if download_url:
            return redirect(download_url)
        return "file not found", 404

    @app.route("/html")
    def html():
        json_data, _ = get_json()
        return render_html(json_data)

    @app.route("/markdown")
    def markdown():
        json_data, _ = get_json()
        return Response(render_markdown(json_data), mimetype="text/plain")

    @app.route("/", defaults={"path": ""})
    @app.route("/<path:_path>")
    def catch_all(_path):
        return send_from_directory(config.get("react.path"), "index.html")

    return app
