import json
from pathlib import Path

import pytest

from vita import create_app

TESTS_PATH = Path(__file__).parent.resolve()
BACKEND_PATH = Path(*TESTS_PATH.parts[:-1])
PROJECT_PATH = Path(*BACKEND_PATH.parts[:-1])


def config_get(key):
    return {"react.path": TESTS_PATH / "test_data"}.get(key)


@pytest.fixture()
def app(mocker):
    mocker.patch("vita.settings.config.get", new=config_get)
    app = create_app()
    app.config.update({"TESTING": True})
    yield app


@pytest.fixture()
def client(app):
    yield app.test_client()


@pytest.fixture()
def with_json(app):
    with open(PROJECT_PATH / "career-test.json", "rb") as file_:
        yield json.loads(file_.read())


def test_download_route_file_found(client, mocker):
    mocker.patch("vita.get_download", return_value="https://dummy.pdf")
    response = client.get("/file", query_string={"format": "pdf"})
    assert response.status_code == 302


def test_download_route_file_not_found(client, mocker):
    mocker.patch("vita.get_download", return_value=None)
    response = client.get("/file", query_string={"format": "pdf"})
    assert response.status_code == 404


def test_html_route(client, with_json, mocker):
    mocker.patch("vita.get_json", return_value=(with_json, ""))
    response = client.get("/html")
    assert response.status_code == 200
    assert '<h1 id="name" class="name">John Doe, Strong Worker // Happy Person</h1>' in response.get_data().decode()


def test_markdown_route(client, with_json, mocker):
    mocker.patch("vita.get_json", return_value=(with_json, ""))
    response = client.get("/markdown")
    assert response.status_code == 200
    assert "# John Doe" in response.get_data().decode()


def test_catchall_route(client):
    response = client.get("/")
    assert response.status_code == 200
    assert response.get_data().decode() == "<html></html>\n"
