set dotenv-load := true

default: frontend_dev

env:
    env | grep -E '^(REACT|VITA).*' | sort

clean: backend_clean frontend_clean

backend_clean:
    rm -rf backend/.eggs backend/build backend/dist backend/vita.egg-info
    cd backend && pipenv --rm

backend_build: backend_clean
    cd backend && pipenv install
    cd backend && pipenv requirements > requirements.txt
    cd backend && pipenv run python -m build -w
    cd backend && rm requirements.txt

backend_dev_install:
    cd backend && pipenv install -d

backend_dev $FLASK_APP="vita:create_app" $FLASK_ENV="developmnt" $FLASK_DEBUG="1":
    cd backend && pipenv run flask run -p 8080

backend_test:
    cd backend && pipenv run pytest -s -v

pipenv +ARGS:
    cd backend && pipenv {{ ARGS }}

frontend_clean:
    rm -rf frontend/node_modules frontend/build frontend/yarn-error.log

frontend_dev_install:
    cd frontend && yarn install

frontend_dev:
    cd frontend && yarn dev

frontend_build:
    cd frontend && yarn install && yarn build

yarn +ARGS:
    cd frontend && yarn {{ ARGS }}

test: backend_test

integration_build:
    cd frontend && rm -rf build && yarn build
    cd backend && VITA__REACT__PATH={{ justfile_directory() }}/frontend/build pipenv run \
        gunicorn -w 1 --threads=4 --timeout=0 -b=":8080" --access-logfile="-" --error-logfile="-" "vita:create_app()"

docker_build env="local" tag="latest" registry="us-central1-docker.pkg.dev/wryfi-mgmt/docker/vita":
    #!/usr/bin/env bash
    pushd backend && pipenv requirements > requirements.txt && popd
    if [[ "{{ env }}" == "local" ]]; then
      build_args="--builder colima"
      tags="-t vita:$(date "+%s") -t vita:{{ tag }}"
    elif [[ "{{ env }}" == "dev" ]]; then
      build_args="--builder multi --platform linux/amd64,linux/arm64 --push"
      tags="-t {{ registry }}:$(date "+%s") -t {{ registry }}:dev"
    elif [[ "{{ env }}" == "prod" ]]; then
      if [[ "{{ tag }}" == "latest" ]]; then
        echo "tag cannot be latest for prod build; please specify a different tag"
        exit 1
      fi
      build_args="--builder multi --platform linux/amd64,linux/arm64 --push"
      tags="-t {{ registry }}:{{ tag }} -t {{ registry }}:latest"
    else
      echo "env must be 'local', 'dev', or 'prod'; please specify a valid environment"
      exit 1
    fi
    if [[ -f ".env.docker.{{ env }}" ]]; then
      export $(cat ".env.docker.{{ env }}" | xargs)
    else
      echo "could not find environment file: .env.docker.{{ env }}; cannot continue!"
      exit 1
    fi
    docker buildx build $build_args \
        --build-arg contactUrl=$REACT_APP_CONTACT_URL \
        --build-arg recaptchaKey=$REACT_APP_RECAPTCHA_KEY \
        --build-arg hostsDev=$REACT_APP_HOSTS_DEV \
        --build-arg hostsProd=$REACT_APP_HOSTS_PROD \
        --build-arg jsonUrlDev=$REACT_APP_JSON_URL_DEV \
        --build-arg jsonUrlProd=$REACT_APP_JSON_URL_PROD \
        --build-arg jsonUrlLocal=$REACT_APP_JSON_URL_LOCAL \
        $tags .
    rm -f backend/requirements.txt

docker_run:
    docker run -it -p 8080:8080 --env-file .env.docker.local vita:latest
